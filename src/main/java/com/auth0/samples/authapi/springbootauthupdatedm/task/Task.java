package com.auth0.samples.authapi.springbootauthupdatedm.task;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.auth0.samples.authapi.springbootauthupdatedm.abteilung.Abteilung;
import com.auth0.samples.authapi.springbootauthupdatedm.kategorie.Kategorie;
import com.auth0.samples.authapi.springbootauthupdatedm.user.ApplicationUser;

/**
 * 
 * Diese KLasse ist das Model für die Tasks
 * @author Artus
 * @date 10.07.2020
 *
 */

@Entity
public class Task {
  
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;

    
    @ManyToOne
    @JoinColumn(name="kategorie_id", referencedColumnName = "id")
    private Kategorie kategorie;

    public Task(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}