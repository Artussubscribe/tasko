package com.auth0.samples.authapi.springbootauthupdatedm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 
 * Dies KLasse startet das ganze Projekt
 * @author Artus
 * @date 10.07.2020
 *
 */

@SpringBootApplication
public class SpringbootAuthUpdatedMApplication {

	@Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	public static void main(String[] args) {
		SpringApplication.run(SpringbootAuthUpdatedMApplication.class, args);
	}

}
