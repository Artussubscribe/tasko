package com.auth0.samples.authapi.springbootauthupdatedm.abteilung;

import org.springframework.data.jpa.repository.JpaRepository;


public interface AbteilungRepository extends JpaRepository<Abteilung, Long> {
}
