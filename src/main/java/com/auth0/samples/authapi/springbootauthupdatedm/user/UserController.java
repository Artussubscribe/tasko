package com.auth0.samples.authapi.springbootauthupdatedm.user;

import java.util.List;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.samples.authapi.springbootauthupdatedm.task.Task;

/**
 * 
 * Diese KLasse ist der Kontroller für den User
 * @author Artus
 * @date 10.07.2020
 *
 */

@RestController
@RequestMapping("/users")
public class UserController {

    private ApplicationUserRepository applicationUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(ApplicationUserRepository applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    
    /**
     * Diese Methode schafft das sich der Benutzer einloggen kann
     * @param user
     */

    @PostMapping("/sign-up")
    public void signUp(@RequestBody ApplicationUser user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        applicationUserRepository.save(user);
    }
    
    /**
     * Mit dieser Methode werden alle Benutzer angezeigt
     * @return
     */
    
    @GetMapping("/all")
    public List<ApplicationUser> getApplicationUser() {
        return applicationUserRepository.findAll();
    }
    
    /**
     * Mit dieser Methode werden Benutzer anhand ihrer ID gelöscht
     * @param id
     */
    
    @DeleteMapping("/all/{id}")
    public void deleteApplicationUser(@PathVariable long id) {
        ApplicationUser userToDel = applicationUserRepository.findById(id).get();
        applicationUserRepository.delete(userToDel);
    }
    
    /**
     * Mit dieser Methode werden Benutzer anhand ihrer ID geändert
     * @param id
     * @param user
     */
    
    @PutMapping("/all/{id}")
    public void editUser(@PathVariable long id, @RequestBody ApplicationUser user) {
        ApplicationUser existingUser = applicationUserRepository.findById(id).get();
        Assert.notNull(existingUser, "Task not found");
        existingUser.setUsername(user.getUsername());
        existingUser.setPassword(user.getPassword());
        applicationUserRepository.save(existingUser);
    }
}
