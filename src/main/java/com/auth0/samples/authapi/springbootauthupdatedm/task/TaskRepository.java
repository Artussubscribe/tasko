package com.auth0.samples.authapi.springbootauthupdatedm.task;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 
 * Diese KLasse ist das Repository
 * @author Artus
 * @date 10.07.2020
 *
 */

public interface TaskRepository extends JpaRepository<Task, Long> {
}