package com.auth0.samples.authapi.springbootauthupdatedm.kategorie;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.auth0.samples.authapi.springbootauthupdatedm.task.Task;


/**
 * 
 * Dies KLasse ist das Model für Kategorie
 * @author Artus
 * @date 10.07.2020
 *
 */

@Entity
public class Kategorie {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@OneToMany(mappedBy = "kategorie")
	private List<Task> tasks = new ArrayList<Task>();
	
	
	public Kategorie() {
		super();
		tasks = new ArrayList<>();
	}
	
	private String kategorie;
	
	
	public String getKategorie() {
		return kategorie;
	}
	public void setKategorie(String kategorie) {
		this.kategorie = kategorie;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	

}
