package com.auth0.samples.authapi.springbootauthupdatedm.kategorie;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller für die Kategorie
 * @author admin
 * @date 10.07.2020
 *
 */

@RestController
@RequestMapping("/kategorie")
public class KategorieController {
	
	private KategorieRepository kategorieRepository;

    public KategorieController(KategorieRepository kategorieRepository) {
        this.kategorieRepository = kategorieRepository;
    }
    
    /**
     * Diese Methode fügt eine neue Kategorie hinzu
     * @param abteilung
     */
    
    @PostMapping
    public void addKategorie(@RequestBody Kategorie kategorie) {
        kategorieRepository.save(kategorie);
    }
    

}