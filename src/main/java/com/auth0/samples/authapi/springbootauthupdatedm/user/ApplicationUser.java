package com.auth0.samples.authapi.springbootauthupdatedm.user;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.auth0.samples.authapi.springbootauthupdatedm.abteilung.Abteilung;

/**
 * 
 * Diese KLasse ist das Model von User
 * @author Artus
 * @date 10.07.2020
 *
 */

@Entity
public class ApplicationUser {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String username;
    private String password;

    @ManyToOne
    @JoinColumn(name="abteilung_id", referencedColumnName = "id")
    private Abteilung abteilung;

    
    public Abteilung getAbteilung() {
		return abteilung;
	}

	public void setAbteilung(Abteilung abteilung) {
		this.abteilung = abteilung;
	}

	public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
