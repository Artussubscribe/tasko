package com.auth0.samples.authapi.springbootauthupdatedm.abteilung;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.samples.authapi.springbootauthupdatedm.task.Task;
import com.auth0.samples.authapi.springbootauthupdatedm.user.ApplicationUser;

/**
 * Controller für die Abteilung
 * @author admin
 * @date 10.07.2020
 *
 */

@RestController
@RequestMapping("/abteilung")
public class AbteilungController {
	
	private AbteilungRepository abteilungRepository;

    public AbteilungController(AbteilungRepository abteilungRepository) {
        this.abteilungRepository = abteilungRepository;
    }
    
    /**
     * Diese Methode fügt eine neue Methode hinzu
     * @param abteilung
     */
    
    @PostMapping
    public void addabteilung(@RequestBody Abteilung abteilung) {
        abteilungRepository.save(abteilung);
    }
    

}
