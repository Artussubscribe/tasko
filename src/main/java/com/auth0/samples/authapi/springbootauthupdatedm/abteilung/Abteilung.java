package com.auth0.samples.authapi.springbootauthupdatedm.abteilung;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.auth0.samples.authapi.springbootauthupdatedm.user.ApplicationUser;

/**
 * 
 * Dies KLasse ist das Model für Abteilung
 * @author Artus
 * @date 10.07.2020
 *
 */

@Entity
public class Abteilung {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String abteilungName;
	
	
	@OneToMany(mappedBy = "abteilung")
	private List<ApplicationUser> users;
	
	
	public Abteilung() {
		super();
		users = new ArrayList<>();
	}
	
	public List<ApplicationUser> getUsers() {
		return users;
	}
	public void setUsers(List<ApplicationUser> users) {
		this.users = users;
	}
	
	public String getAbteilungName() {
		return abteilungName;
	}
	public void setAbteilungName(String abteilungName) {
		this.abteilungName = abteilungName;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	

}
