package com.auth0.samples.authapi.springbootauthupdatedm.user;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 
 * Diese KLasse ist das Repository von User
 * @author Artus
 * @date 10.07.2020
 *
 */

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
	/**
	 * spezielle Abfrage
	 * @param username
	 * @return
	 */
    ApplicationUser findByUsername(String username);
}
