package com.auth0.samples.authapi.springbootauthupdatedm.kategorie;

import org.springframework.data.jpa.repository.JpaRepository;


public interface KategorieRepository extends JpaRepository<Kategorie, Long> {
}
